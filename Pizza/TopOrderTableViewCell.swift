//
//  ToppingTableViewCell.swift
//  Pizza
//
//  Created by Hari Krishna Bista on 12/9/16.
//  Copyright © 2016 meroApp. All rights reserved.
//

import UIKit

class TopOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblToppingName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
