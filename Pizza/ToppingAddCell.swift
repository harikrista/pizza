//
//  ToppingAddCell.swift
//  Pizza
//
//  Created by Hari Krishna Bista on 12/10/16.
//  Copyright © 2016 meroApp. All rights reserved.
//

import UIKit

class ToppingAddCell: UITableViewCell {

    @IBOutlet weak var txtTopping: UITextField!
    @IBOutlet weak var btnAddMore: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
