//
//  Order.swift
//  Pizza
//
//  Created by Hari Krishna Bista on 12/11/16.
//  Copyright © 2016 meroApp. All rights reserved.
//

import UIKit



class Order: Hashable, Equatable {
    
    static let AVAILABLE_TOPPINGS:[String:UInt64] = [
        "salami":0,
        "pepperoni":1,
        "four cheese":2,
        "mozzarella cheese":4,
        "bacon":8,
        "beef":16,
        "sausage":32,
        "mushrooms":64,
        "italian sausage":128,
        "black olives":256,
        "chicken":512,
        "pineapple":1024,
        "ham":2048,
        "jalapenos":4096,
        "green peppers":8192,
        "canadian bacon":16384,
        "diced white onions":32768,
        "cheddar cheese":65536,
        "diced tomatoes":262144,
        "alredo sauce":524288,
        "onions":1048576,
        "hot peppers":2097152,
        "sliced breaded chicken breast":4194304,
        "artichokes":8388608,
        "anchovies":16777216,
        "feta cheese":33554432,
        "garlic basil oil":67108864,
        "giant pepperoni":134217728,
        "parmesan parsley":268435456,
        "sliced roma tomatoes":536870912,
        "carmelized red onion":1073741824,
        "refried beans":2147483648,
        "roasted red pepper":4294967296,
        "rosa grande pepperoni":8589934592,
        "fresh basil":17179869184,
        "lettuce":34359738368
    ];
    
    var toppings:[String];
    
    var repeatCount = 0;
    var isFav = false;
    var uuid:UInt64 = 0;
    
    init(newToppings:[String]) {
        self.toppings=newToppings;
    }
    
    var hashValue: Int {
        return 0;
    }
    
    func setUUID() {
        for topping in self.toppings {
            //            self.uuid = self.uuid + pow(2,Order.AVAILABLE_TOPPINGS.index(of: topping)!);
            self.uuid = self.uuid + Order.AVAILABLE_TOPPINGS[topping]!;
        }
    }
    
    public static func ==(lhs: Order, rhs: Order) -> Bool{
        
        //        guard lhs.toppings.count == rhs.toppings.count else {
        //            return false;
        //        }
        //
        //        for i in 0...(rhs.toppings.count-1) {
        //            if(!(lhs.toppings[i] == rhs.toppings[i])){
        //                return false;
        //            }
        //        }
        
        return lhs.uuid == rhs.uuid;
    }
}
