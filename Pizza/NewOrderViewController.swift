//
//  NewToppingViewController.swift
//  Pizza
//
//  Created by Hari Krishna Bista on 12/9/16.
//  Copyright © 2016 meroApp. All rights reserved.
//

import UIKit

extension UITableView {
    
    func tableViewScrollToBottom(animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: animated)
            }
        }
    }
}

class NewOrderViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var toppings:[CDTopping]=[];
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.tableView.delegate=self;
        self.tableView.dataSource=self;
    }
    
    // tableview delegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.toppings.count + 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToppingAddCell", for: indexPath) as! ToppingAddCell;
        
        cell.txtTopping.isEnabled = false;
        cell.btnAddMore.setImage(UIImage(named:"delete-icon"), for: .normal);

        if(self.toppings.count == indexPath.row){
            cell.txtTopping.isEnabled = true;
            cell.btnAddMore.setImage(UIImage(named:"add-icon"), for: .normal);
            cell.txtTopping.text = "";
            cell.txtTopping.becomeFirstResponder();
        }else{
            cell.txtTopping.text = self.toppings[indexPath.row].toppingName;
        }
        
        cell.btnAddMore.tag = indexPath.row;
        cell.btnAddMore.addTarget(self, action: #selector(NewOrderViewController.connected(sender:)), for: .touchUpInside);
        
        return cell;
    }
    
    func connected(sender: UIButton){

        let indexPath = IndexPath(row: sender.tag, section: 0);
        
        if let cell = self.tableView.cellForRow(at: indexPath) as? ToppingAddCell {

            if(self.toppings.count == sender.tag){
                
                let toppingToAdd = cell.txtTopping.text;

                if(!(toppingToAdd?.isEmpty)!) {
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate;
                    let newTopping = appDelegate.dataController.createNewCDTopping()
                    newTopping.toppingName = toppingToAdd;
                    self.toppings.append(newTopping);
                    
                    self.tableView.reloadData();
                    self.tableView.tableViewScrollToBottom(animated: true);
                }
                
            }else{
                self.toppings.remove(at: sender.tag);
                self.tableView.deleteRows(at: [indexPath], with: .left);
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.tableView.reloadData();
                }
            }
        }
    }

    @IBAction func btnSaveTapped(_ sender: UIButton) {
        
        let indexPath = IndexPath(row: self.toppings.count, section: 0);
        
        if let cell = self.tableView.cellForRow(at: indexPath) as? ToppingAddCell {
            
            if(cell.txtTopping.text?.isEmpty)!{
                
            }else{
                let appDelegate = UIApplication.shared.delegate as! AppDelegate;
                let newTopping = appDelegate.dataController.createNewCDTopping()
                newTopping.toppingName = cell.txtTopping.text!;
                self.toppings.append(newTopping);
            }
        }
        
        self.performSegue(withIdentifier: "unwindFromNewToPizza", sender: self);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true);
    }

    @IBAction func btnCancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
