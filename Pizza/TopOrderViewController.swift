//
//  ToppingResultViewController.swift
//  Pizza
//
//  Created by Hari Krishna Bista on 12/9/16.
//  Copyright © 2016 meroApp. All rights reserved.
//

import UIKit

class TopOrderViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var topResult:TopResultGenerator?;

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        
        self.topResult = TopResultGenerator();
        
        self.tableView.isHidden=true;
        self.activityIndicator.startAnimating();
        
        self.tableView.estimatedRowHeight = 70.0;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        func problemBlock(){
            for _ in 0...4000{
                print("hari hari");
            }
        }
        
        let start = NSDate(); // <<<<<<<<<< Start time
        
//        self.topResult?.findAllToppings();
//        
//        let end = NSDate();   // <<<<<<<<<<   end time
//        let timeInterval: Double = end.timeIntervalSince(start as Date); // <<<<< Difference in seconds (double)
//
//        print("Time to evaluate problem \(timeInterval) seconds");
        
        self.topResult?.loadJsonData(completion: { (isParsed:Bool) in
            
            let end = NSDate();   // <<<<<<<<<<   end time
            let timeInterval: Double = end.timeIntervalSince(start as Date); // <<<<< Difference in seconds (double)
            
            print("Time to evaluate problem \(timeInterval) seconds");
            
            self.activityIndicator.stopAnimating();
            self.tableView.isHidden=false;

            if(isParsed){
                self.tableView.reloadData();
                print("here result");
            }else{
                print("parse failure");
            }
        });
        
        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.topResult?.topMostOrders?.count)!;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopOrderTableViewCell") as! TopOrderTableViewCell;
        
        let ordr:Order = (self.topResult?.topMostOrders![indexPath.row])!;
        
        cell.lblCount.text = "\(ordr.repeatCount)";
        
        var toppingss = "";
        
        for str in ordr.toppings {
            toppingss = toppingss + str + "\n";
        }
        
        cell.lblToppingName.text = toppingss;
        
        cell.lblNumber.text="\(indexPath.row+1).";
        return cell;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnFilterTapped(_ sender: UIBarButtonItem) {
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        
        let actionAll: UIAlertAction = UIAlertAction(title: "All", style: .default)
        { action -> Void in
            self.topResult?.updateTopLimit(newTopLimit: -1);
            self.tableView.reloadData();
        }
        
        let action20: UIAlertAction = UIAlertAction(title: "20", style: .default)
        { action -> Void in
            self.topResult?.updateTopLimit(newTopLimit: 20);
            self.tableView.reloadData();
        }
        
        let action10: UIAlertAction = UIAlertAction(title: "10", style: .default)
        { action -> Void in
            self.topResult?.updateTopLimit(newTopLimit: 10);
            self.tableView.reloadData();
        }
        
        let action5: UIAlertAction = UIAlertAction(title: "5", style: .default)
        { action -> Void in
            self.topResult?.updateTopLimit(newTopLimit: 5);
            self.tableView.reloadData();
        }
        
        actionSheetController.addAction(actionAll);
        actionSheetController.addAction(action20);
        actionSheetController.addAction(action10);
        actionSheetController.addAction(action5);

        actionSheetController.addAction(cancelActionButton)

        self.present(actionSheetController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
