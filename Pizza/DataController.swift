//
//  DataController.swift
//  PhotoFeed
//
//  Created by Mike Spears on 2016-01-10.
//  Copyright © 2016 YourOganisation. All rights reserved.
//

import Foundation
import CoreData

class DataController {
    
    let managedObjectContext: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext) {
        self.managedObjectContext = moc ;
    }

    convenience init?() {
        
        guard let modelURL = Bundle.main.url(forResource: "Pizza", withExtension: "momd") else {
            return nil
        }
        
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            return nil
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        moc.persistentStoreCoordinator = psc
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let persistantStoreFileURL = urls[0].appendingPathComponent("Pizza.sqlite")
        
        do { 
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: persistantStoreFileURL, options: nil)
        } catch {
            fatalError("Error adding store.")
        }
        
        self.init(moc: moc);
    }
    
    func getSavedCDOrders()->[CDOrder] {
        let tagsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CDOrder")
        var fetchedTags: [CDOrder]!;
        do {
            fetchedTags = try self.managedObjectContext.fetch(tagsFetch) as! [CDOrder]
        } catch {
            fatalError("fetch failed")
        }
        return fetchedTags;
    }
    
    func createNewCDOrder() -> CDOrder {
        let cdorder = NSEntityDescription.insertNewObject(forEntityName: "CDOrder", into: self.managedObjectContext) as! CDOrder;
        return cdorder;
    }
    
    func createNewCDTopping() -> CDTopping {
        return NSEntityDescription.insertNewObject(forEntityName: "CDTopping", into: self.managedObjectContext) as! CDTopping;
        
    }
    
    func saveContext() {
        do {
            try self.managedObjectContext.save()
        } catch {
            fatalError("couldn't save context")
        }
    }
}
