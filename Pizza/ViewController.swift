//
//  ViewController.swift
//  Pizza
//
//  Created by Hari Krishna Bista on 12/9/16.
//  Copyright © 2016 meroApp. All rights reserved.
//
import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var orders:[CDOrder]?;
    var currDataController:DataController?;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.currDataController = (UIApplication.shared.delegate as! AppDelegate).dataController;

        // getting order test from coredata
        self.orders = self.currDataController?.getSavedCDOrders();

        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        
        self.tableView.estimatedRowHeight = 70.0;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.orders?.count)!;
    }
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            self.orders?.remove(at: indexPath.row);
            tableView.deleteRows(at: [indexPath], with: .left);
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomOrderTableViewCell") as! CustomOrderTableViewCell;

        let ordr = self.orders?[indexPath.row];

        var toppingss = "";
        for  cdTopping in (ordr?.toppings)! {
            let cdToppingtemp = cdTopping as! CDTopping;
            toppingss += cdToppingtemp.toppingName! + "\n";
        }
        cell.lblToppingName.text = toppingss;
        
        if(ordr?.isFav)!{
            cell.btnFav.setImage(UIImage(named:"fav-icon"), for: .normal);
        }else{
            cell.btnFav.setImage(UIImage(named:"unfav-icon"), for: .normal);
        }
        
        cell.btnFav.tag = indexPath.row;
        cell.btnFav.addTarget(self, action: #selector(ViewController.connected(sender:)), for: .touchUpInside);
        
        cell.lblNumber.text="\(indexPath.row+1).";
        return cell;
    }
    
    func connected(sender: UIButton){
        // update the coredata managed context when user selects favorite
        if let order = self.orders?[sender.tag] {
            order.isFav = !(order.isFav);
            self.currDataController?.saveContext();
            self.tableView.reloadData();
        }
    }
    
//    func saveOrder() {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate;
//        appDelegate.dataController.saveContext();
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToMenu(segue: UIStoryboardSegue) {
        
        let srcViewCOn = segue.source as! NewOrderViewController;
        
        if(srcViewCOn.toppings.count>0){
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate;
            let newCDOrder = appDelegate.dataController.createNewCDOrder();
            
            newCDOrder.addToToppings(NSSet(array: srcViewCOn.toppings));
            self.orders?.append(newCDOrder);
            self.tableView.reloadData();
            
            self.currDataController?.saveContext();
        }
    }
    
    @IBAction func btnAddNewToppingTapped(_ sender: UIBarButtonItem) {
        
    }
    
}

