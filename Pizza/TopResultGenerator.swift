//
//  ToppingGenerator.swift
//  Pizza
//
//  Created by Hari Krishna Bista on 12/9/16.
//  Copyright © 2016 meroApp. All rights reserved.
//

import UIKit

class TopResultGenerator {
    
    private var TopLimit=20;
    
    var allSortedOrders:[Order]?;
    
    public var topMostOrders:[Order]?;
    
    
    // storing data into plist
    
    func saveAllSortedOrders() {
        
    }
    
    func updateTopLimit(newTopLimit:Int) {
        self.TopLimit=newTopLimit;
        
        if(self.TopLimit == -1 || self.TopLimit > (self.allSortedOrders?.count)!){
            self.TopLimit = (self.allSortedOrders?.count)!;
        }
        
        self.TopLimit=self.TopLimit-1;
        
        self.topMostOrders?.removeAll();
        for i in 0...TopLimit {
            self.topMostOrders?.append((self.allSortedOrders?[i])!);
        }
    }
    
    init() {
        self.allSortedOrders = [];
        self.topMostOrders = [];
    }
    
    // loads large json in different thread and sends results to main thread when completed
    // this is needed to make UI not freezing when performing huge task.
    
//    func findAllToppings() {
//        var dictToppings:[String:Int]=[:];
//        
//        if let path = Bundle.main.path(forResource: "Orders", ofType: "json") {
//            do {
//                let jsonData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe)
//                do {
//                    let Orders  = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! [NSDictionary];
//                    
//                    for jsnOrder in Orders {
//                        
//                        let toppingsss = jsnOrder["toppings"] as! [String];
//                        
//                        for topping in toppingsss{
//                            let compareClossure = { (item: (key: String, value: Int)) -> Bool in
//                                if(topping == item.key){
//                                    return true;
//                                }else{
//                                    return false;
//                                }
//                            };
//                            
//                            if(dictToppings.contains(where: compareClossure)){
//                                dictToppings[topping] = dictToppings[topping]! + 1;
//                            }else{
//                                dictToppings[topping] = 1;
//                            }
//                        }
//                    }
//                    
//                } catch {
//                    print("json parse error");
//                }
//            } catch {
//                print("contentsOfFile error");
//            }
//        }else{
//            print("file not found");
//        }
//        
//        let sortingLogic = {(
//            item1: (name1:String,count1:Int),
//            item2: (name2:String,count2:Int))
//            -> Bool in
//            return item2.count2 < item1.count1;
//        }
//        
//        let sorted = dictToppings.sorted (by: sortingLogic);
//
//        for (key, value) in sorted {
//            print(key);
//        }
//        
//        print("totalToppings: \(sorted.count)");
//        
//        let temp:Int64 = 68719476736;
//        
//        print(temp);
//    }
    
    func loadJsonData(completion: @escaping (_ finished:Bool) -> Void) {
        OperationQueue().addOperation {
            
            var dictOrders:[Order:Int]=[:];
            
            if let path = Bundle.main.path(forResource: "Orders", ofType: "json") {
                do {
                    let jsonData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe)
                    do {
                        let Orders  = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! [NSDictionary];
                        
                        for jsnOrder in Orders {
                            
                            let order = Order(newToppings: jsnOrder["toppings"] as! [String]);
                            
                            order.setUUID();
                            
                            let compareClossure = { (orderDict: (key: Order, value: Int)) -> Bool in
                                if(order == orderDict.key){
                                    return true;
                                }else{
                                    return false;
                                }
                            };
                            
                            // storing unique order and storing count of repeated order
                            if(dictOrders.contains(where: compareClossure)){
                                dictOrders[order] = dictOrders[order]! + 1;
                            }else{
                                dictOrders[order] = 1;
                            }
                        }
                        
                    } catch {
                        print("json parse error");
                        OperationQueue.main.addOperation({ () -> Void in
                            completion(false);
                        });
                    }
                } catch {
                    print("contentsOfFile error");
                    OperationQueue.main.addOperation({ () -> Void in
                        completion(false);
                    });
                }
            }else{
                print("file not found");
                
                OperationQueue.main.addOperation({ () -> Void in
                    completion(false);
                });
            }
            
            // sorting the dictionary with highest repeated order first
            let sortingLogic = {(
                item1: (name1:Order,count1:Int),
                item2: (name2:Order,count2:Int))
                -> Bool in
                return item2.count2 < item1.count1;
            }
            
            let sorted = dictOrders.sorted (by: sortingLogic);
            
            for (key, value) in sorted {
                key.repeatCount = value;
                self.allSortedOrders?.append(key);
            }
            
            // preparing the only top 20 orders first
            self.updateTopLimit(newTopLimit: 20);
            
            OperationQueue.main.addOperation({ () -> Void in
                completion(true);
            });
        }
    }
    
}
